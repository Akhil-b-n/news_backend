require ('dotenv').config()
const express = require('express')
const app = express()
const cors = require("cors")
const cookieParser = require('cookie-parser');
app.use(cors({credentials:true, origin:true}))
app.use(express.json())
app.use(cookieParser());
const Author = require('./routes/authorroutes')
const news =require('./routes/newsroute')
const user=require('./routes/userroutes')
const authroutes = require('./routes/authroutes')
const reviewroutes = require('./routes/reviewroutes')
const mongoose = require("mongoose")
const port = 3000
const url= process.env.mongo_url
app.use('/news',news)
app.use('/author',Author)
app.use('/user',user)
app.use('/auth',authroutes)
app.use('/reviews', reviewroutes)


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


main().then(console.log("connected")).catch(err => console.log(err));

async function main() {
  await mongoose.connect(process.env.mongo_url);

}

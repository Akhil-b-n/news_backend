const express =require ("express")
const { getuser, getuserid, postuser, patchuser, deleteuser } = require("../controllers/usercontroller")
const router =express.Router()

router.get('/',getuser)
router.get('/:userId',getuserid)
router.post('/',postuser)
router.patch('/:userId',patchuser)
router.delete('/:userId',deleteuser)

module.exports = router
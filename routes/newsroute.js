const express = require('express')
const { getnews, getnewsid, patchnews, postnews, deletenews } = require('../controllers/controller')
const router = express.Router()

router.get('/',getnews)
router.get('/:newsId',getnewsid )
router.patch('/:newsId',patchnews)
router.post('/',postnews)
router.delete('/:newsId',deletenews)

module.exports = router
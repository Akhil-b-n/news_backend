const express = require('express')
const { Getreview, addreview } = require('../controllers/reviewcontroller')
const { protect } = require('../Middleware/Protect')
const router = express.Router()

router.get('/', Getreview)
router.post('/',protect, addreview)

module.exports = router
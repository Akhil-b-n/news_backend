const express = require('express')
const { login, verify, logOut } = require('../controllers/authcontroller')
const router = express.Router()

router.post('/login',login)
router.get('/verify',verify)
router.get('/logout',logOut )


module.exports = router
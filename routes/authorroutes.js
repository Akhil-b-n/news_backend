const express = require('express')
const { getAuthor, getAuthorid, patchAuthor, postAuthor, deleteAuthor } = require('../controllers/authorcontroller')
const router = express.Router()

router.get('/',getAuthor)
router.get('/:AuthorId',getAuthorid)
router.patch('/:AuthorId',patchAuthor)
router.post('/',postAuthor)
router.delete('/:AuthorId',deleteAuthor)

module.exports = router
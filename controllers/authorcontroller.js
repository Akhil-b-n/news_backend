const Author = require("../model/authormodel")

const getAuthor = async( req ,res)=>{
    const author = await Author.find({})
    res.json(author)
 }
 
 const getAuthorid = async(req,res)=>{
    const author = await Author.findById(req.params.AuthorId)
    res.json(author)
 }
 
 const patchAuthor = async(req,res)=>{
    const author = await Author.findByIdAndUpdate(req.params.AuthorId,req.body, {new:true})
    res.json(author)
    
 }
 
 const postAuthor =async(req,res)=>{
    const author = new Author(req.body)
    await author.save()
    res.json(author)
 }
 
 const deleteAuthor = async(req,res)=>{
    await Author.findByIdAndDelete(req.params.AuthorId)
    res.send("deleted")
 }
 
 module.exports= {getAuthor, getAuthorid,patchAuthor,postAuthor,deleteAuthor}
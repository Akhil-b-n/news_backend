const Users = require("../model/usermodel");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const login = async(req,res) =>{
    const data = req.body
    
    const user = await Users.findOne({ email: data.email }).exec();
    
    if (!user) {
        return res.status(401).send("Invalid email or password" );
    }
    
    const passwordMatch = bcrypt.compareSync(data.password, user.password);

    if(passwordMatch){
        const token = jwt.sign({ _id: user._id, email: user.email },process.env.jwt_token, {expiresIn:"1hr"});
         res.cookie('token',token,{httpOnly:true})
         res.send("Log in successful")
        

    }
    else{
        return res.status(401).send("Invalid password")
    }}

    const verify= async(req,res)=>{
        if(req.cookies.token){
            try{
                const payload = jwt.verify(req.cookies.token,process.env.jwt_token)
                console.log(payload)
                console.log()
                res.send('Logged In')
            }
            catch(error){
                res.status(401).send('Unauthorised access')
            }
        }
        else{
            res.status(401).send('Unauthorised access')
        }
    }

    const logOut = async(req,res)=>{
        res.cookie('token','',{expires: new Date(0), httpOnly:true})
        res.send("logged out")
    }

module.exports={ login, verify,logOut}
const Review = require('../model/reviewmodel')

const Getreview = async(req,res) =>{
    const review = await Review.find({})
   res.json(review)
}

const addreview = async(req,res)=>{
    try{
        const review = new Review({...req.body,user:req.user._id})
        await review.save()
        res.json(review)  
    }
    catch{
        res.status(401).send('unauthorized access')
    }
}

module.exports ={Getreview, addreview}
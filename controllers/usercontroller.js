const User = require("../model/usermodel")
const bcrypt = require('bcrypt');
const saltRounds = 10;

const getuser = async(req,res) =>{
    const user = await User.find
    res.json(user)
}

const getuserid = async(req,res) =>{
    const user = await User.findById(req.params.id)
    res.json(user)
}
const postuser = async(req,res) =>{
    const data = req.body
    const password = req.body.password
    const hash = bcrypt.hashSync(password, saltRounds);
    const user = new User({... data, password:hash})
    await user.save()
    res.json({message:"user added successfully" , user})
    }

const patchuser = async(req,res)=>{
    const user = await User.findByIdAndUpdate(req.params.id,req.body, {new:true})
    res.json(user)
}

const deleteuser = async(req,res)=>{
    const user = await User.findByIdAndDelete(req.params.id)
    res.send("deleted")
}

module.exports ={getuser, patchuser, getuserid, postuser,deleteuser }
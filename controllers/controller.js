const News = require("../model/newsmodel")

const getnews = async(req,res)=>{
   const news = await News.find({})
   res.json(news)
}

const getnewsid = async(req,res)=>{
   const news = await News.findById(req.params.newsId)
   res.json(news)
}

const patchnews = async(req,res)=>{
   const news = await News.findByIdAndUpdate(req.params.newsId,req.body, {new:true})
   res.json(news)
   
}

const postnews =async(req,res)=>{
   const news = new News(req.body)
   await news.save()
   res.json(news)
}

const deletenews = async(req,res)=>{
   await News.findByIdAndDelete(req.params.newsId)
   res.send("deleted")
}

module.exports= {getnews, getnewsid,patchnews,postnews,deletenews}
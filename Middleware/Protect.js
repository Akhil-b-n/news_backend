const jwt = require('jsonwebtoken');
const Users = require("../model/usermodel")
const protect = async(req,res,next) =>{
    if(req.cookies.token){
        try{
            const payload = jwt.verify(req.cookies.token,process.env.jwt_token)
            console.log(payload)
            const user = await Users.findById(payload._id).exec();
            console.log('user')
            if (!user) {
                return res.status(401).send('Unauthorised: User not found');
            }
            req.user=user
            next()
        }
        catch(err){res.status(401).send('Unauthorised ')
             console.log(err)}
    }
    else{
        res.status(401).send('Unauthorised access')
    }
}
module.exports = {protect};
const mongoose =require('mongoose')

const newsSchema = new mongoose.Schema({
    title: String,
    thumbnail: String,
    description: String,
    article:String
  });


  const News = mongoose.model('News', newsSchema);

  module.exports = News
const mongoose = require("mongoose")

const reviewSchema = new mongoose.Schema({
   review:{
    type:String,
    required:true},
   user:{
    type: mongoose.ObjectId,
    ref: 'Users',
    required:true
   },
   news:{
    type: mongoose.ObjectId,
    ref: 'News',
    required:true
   }
});

const Review = mongoose.model('Review', reviewSchema);
module.exports = Review;

const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({
    username: String,
    email: String,
    password: String,
    phone: Number
});

const Users = mongoose.model('Users', userSchema);
module.exports = Users;

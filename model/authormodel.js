const mongoose =require('mongoose')

const authorSchema = new mongoose.Schema({
    name: String,
    description:String,
    image:String,
    location:String
  });


  const Author = mongoose.model('Author', authorSchema);

  module.exports = Author